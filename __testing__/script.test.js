const googleSearch= require('../script');
const mockDB=[
    'aya.com',
    'samir.com',
    'hotmail.com'
];
describe('google search',()=>{
    it('should return empty array',()=>{
        expect(googleSearch('ttt.com',mockDB)).toEqual([]);
        expect(googleSearch(undefined,mockDB)).toEqual([]);
    })
    it('should return aya.com',()=>{
       expect(googleSearch('aya.com',mockDB)).toEqual(['aya.com']);
   })
});